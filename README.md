# Text driven car

The idea of this project is to use machine learning to drive and to avoid collision of objects (in front of the vehicle).
A device with CUDA capable GPU and webcamera is needed to detect text. The easyOCR library is able to identify all kind
of letters but the program will only send messages over socket to the jetson if they are of interest. Here is a picture of the words
used to control the robot.

![command_notes](command_notes.jpg)



The jetson is also loaded with a repurposed image recognition model with two classifications one for identifying
when the front of the vehicle is blocked, and one for identifying when it is clear. If the robot is driving forward
towards an object it will stop and the forward command will be disabled until the obstacle is out of vision for the robot.
By utilizing threads the jetson nano is able to both receive input over socket and detect collision simultaneously.   

The performance of the object detection can be improved by training the model further. This can be done by taking 
more picture and add them to the dataset folders free and blocked and then run the training_model notebook. Keep in mind
that if you are using different image sizes these must be resized using torchvisions datasets function resize.


# Installation 
Flash the SD Card
Download SD card image from 
https://developer.nvidia.com/embedded/l4t/r32_release_v6.1/jeston_nano/jetson-nano-jp46-sd-card-image.zip 

## On windows 
Use tool such as Etcher or Rufus to flash image to sd card

## On Linux
sudo dd if=path/to/image of=/path/to/device bs=1M status=progress
Alternatively use some GUI such as https://github.com/LinArcX/kindd


On first boot you can either connect peripherals such as keyboard, monitor and mouse and enter 

```
sudo systemctl set-default graphical.target
``` 

in the terminal start a desktop environment. Alternatively a software such as
[PuTTy](https://www.putty.org/) can be used to connect through serial (a microusb cable)
is nescessary for this. The following command can be used to connect to a wireless network

```
sudo nmcli dev wifi connect network-ssid password "network-password"
``` 

After the jetson has been connected to a network it can be accessed through ssh
in the future.


# Dependencies (Installed on Jetson Nano)
### Install NodeJS for Jupyter notebook

```
curl -fsSL https://deb.nodesource.com/setup_14.x | sudo -E bash - &&\
sudo apt-get install -y nodejs libffi-dev
```

### Install Jupyter Lab

```
sudo python3 -m pip install jupyter jupyterlab
jupyter labextension install @jupyter-widgets/jupyterlab-manager
```

### Install pytorch
```
wget https://nvidia.box.com/shared/static/p57jwntv436lfrd78inwl7iml6p13fzh.whl -O torch-1.8.0-cp36-cp36m-linux_aarch64.whl
sudo apt-get install python3-pip libopenblas-base libopenmpi-dev libomp-dev
pip3 install Cython
pip3 install numpy torch-1.8.0-cp36-cp36m-linux_aarch64.whl
```
### Install torchvision

```
sudo apt-get install libjpeg-dev zlib1g-dev libpython3-dev libavcodec-dev libavformat-dev libswscale-dev

git clone --branch release/0.9 https://github.com/pytorch/vision

cd torchvision

export BUILD_VERSION=0.9.0

python3 setup.py install –user

cd ../

pip install pillow<7
```

### Install jetbot python package
```
git clone https://github.com/NVIDIA-AI-IOT/jetbot
cd jetbot
sudo python3 setup.py install
```

### Clone this repo inside jetbot folder

git clone https://gitlab.com/Douglas_Johansson/jetson_nano.git
cd jetson_nano

### To run the notebook server:
```
jupyter lab --ip=0.0.0.0 --no-browser --allow-root
```

# On device running textdetection 

First ensure CUDA is avaiable

Linux: https://docs.nvidia.com/cuda/cuda-installation-guide-linux/index.html
Windows: https://docs.nvidia.com/cuda/cuda-installation-guide-microsoft-windows/index.html

### Install opencv(4.5.4.60 – Later version does not work with easyocr)
```
pip install opencv-python==4.5.4.60
```
### Install easyocr
```
pip install easyocr
```

# How to run
Start off by writing Forward, Backward, Left, Right, Stop and Exit on some pieces of paper.

Then start running the textdetection script

```
python3 textdetection.py
```
Connect to the notebook using the jetson hostname/ip:8888 in a webbrowser. 
The ip can be found using ip addr in the jetson terminal window.
If you need a login token enter jupyter notebook lis in the terminal and copy it as password.
In jupyter notebook open up the robot notebook, run all the cells in order except the last one.
A window should open with the camera feed. Put one of the paper pieces in front of the camera and watch the robot move.


## Maintainer
@Douglas_Johansson


