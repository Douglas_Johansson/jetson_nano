import cv2
import easyocr
import socket
# Video source
cap = cv2.VideoCapture(0)
# Use easy ocr library to detect english text, please note that the device needs CUDA drivers,
# currently only supported for nvidia gpus. running this on CPU is extremly slow
reader = easyocr.Reader(['en'], gpu=True)

# Text strings used to control the robot
commands = ['Forward', 'Backward', 'Left', 'Right', 'Exit', 'Stop']
socket = socket.socket()
print("Socket successfully created")

# Port socket should listen on
port = 5050
socket.bind(('', port))

# put the socket into listening mode
socket.listen()
(connection, address) = socket.accept()
while True:
    esc, frame = cap.read()
    cv2.imshow("Webcam", frame)

    result = reader.readtext(frame, detail=0)
    print(result)
    # If some text is detected
    if len(result) == 1:
        # If text matches any of our commands
        if result[0] in commands:
            # Send text over socket
            connection.send(result[0].encode())
            if result[0] == 'Exit':
                connection.close()
    if cv2.waitKey(1) == 27:
        print("Exiting")
        connection.close()
        break
cap.release()